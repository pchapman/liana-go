package pointerutil

// Functions useful for assinging pointers to structs directly from the results of functions that return values.
// 
// type myStruct struct {
//     value *string
// }
// 
// func returnsString() string {
//     return "foo"
// }
// 
// func main() {
//     ms := myStruct {
//         value: PtrForString(returnsString()),
//     }
// }

func PtrForFloat32(f float32) *float32 {
    return &f
}

func PtrForFloat64(f float64) *float64 {
    return &f
}

func PtrForInt(i int) *int {
    return &i
}

func PtrForInt8(i int8) *int8 {
    return &i
}

func PtrForInt16(i int16) *int16 {
    return &i
}

func PtrForInt32(i int32) *int32 {
    return &i
}

func PtrForInt64(i int64) *int64 {
    return &i
}

func PtrForString(s string) *string {
    return &s
}
