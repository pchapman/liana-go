package updater

import (
	"encoding/json"
	"encoding/xml"
	yaml "gopkg.in/yaml.v2"
	"testing"
)

// Tests cases for deserializing DB updateer objects from JSON, XML and YAML.  These test cases also give examples of
// how one would represent these objects in the various formats.

// Tests unmarshalling DbVersions from JSON
func TestVersionsJSON(t *testing.T) {
	blob := `
		{
			"versionTable": {
				"insert": "INSERT INTO ams_db_version (\nmajor, minor, revision, updated\n) VALUES (\n$1, $2, $3, current_timestamp\n);",
				"select": "SELECT major, minor, revision\nFROM ams_db_version\nORDER BY major DESC, minor DESC, revision DESC\nLIMIT 1"
			},
			"versions": [
				{"major": 1, "minor": 0, "revision": 0, "updateid": "1.0.0"},
				{"major": 1, "minor": 0, "revision": 1, "updateid": "1.0.1"}
			]
		}`

	versions := new(DbVersions)
	err := json.Unmarshal([]byte(blob), versions)
	validateVersions(t, versions, err)
}

// Tests unmarshalling DbVersions from XML
func TestVersionsXML(t *testing.T) {
	blob := `
		<versions>
			<versionTable>
				<insert>
					INSERT INTO ams_db_version (
						major, minor, revision, updated
					) VALUES (
						$1, $2, $3, current_timestamp
					);
				</insert>
				<select>
					SELECT major, minor, revision
					FROM ams_db_version
					ORDER BY major DESC, minor DESC, revision DESC
					LIMIT 1
				</select>
			</versionTable>
			<version major="1" minor="0" revision="0" updateid="1.0.0"/>
			<version major="1" minor="0" revision="1" updateid="1.0.1"/>
		</versions>`

	versions := new(DbVersions)
	err := xml.Unmarshal([]byte(blob), versions)
	validateVersions(t, versions, err)
}

// Tests unmarshalling DbVersions from YAML
func TestVersionsYAML(t *testing.T) {
	blob := `
versionTable:
  insert: |
    INSERT INTO ams_db_version (
      major, minor, revision, updated
    ) VALUES (
      $1, $2, $3, current_timestamp
    );
  select: |
    SELECT major, minor, revision
    FROM ams_db_version
    ORDER BY major DESC, minor DESC, revision DESC
    LIMIT 1
versions:
  - major: 1
    minor: 0
    revision: 0
    updateid: 1.0.0
  - major: 1
    minor: 0
    revision: 1
    updateid: 1.0.1`

	versions := new(DbVersions)
	err := yaml.Unmarshal([]byte(blob), versions)
	validateVersions(t, versions, err)
}

// Validates DbVersions data un-marshaled from the various formats.
func validateVersions(t *testing.T, versions *DbVersions, err error) {
	if err != nil {
		t.Errorf("Error unmarshalling versions: %s", err)
	}

	if versions.VersionTable.Insert == "" {
		t.Errorf("No versions table insert statement read")
	}
	if versions.VersionTable.Select == "" {
		t.Errorf("No versions table select statement read")
	}
	if versions.Versions == nil {
		t.Errorf("No versions read")
	}
	i := len(versions.Versions)
	if i != 2 {
		t.Errorf("Expected 2 versions, but found %d", i)
	}

	v := versions.Versions[0]
	if v.Major != 1 {
		t.Errorf("Expected Major number 1 in item 0, found %d", v.Major)
	}
	if v.Minor != 0 {
		t.Errorf("Expected Minor number 0 in item 0, found %d", v.Minor)
	}
	if v.Revision != 0 {
		t.Errorf("Expected Revision number 0 in item 0, found %d", v.Major)
	}
	if v.UpdateId == "" {
		t.Errorf("No update id read in item 0")
	}

	v = versions.Versions[1]
	if v.Major != 1 {
		t.Errorf("Expected Major number 1 in item 1, found %d", v.Major)
	}
	if v.Minor != 0 {
		t.Errorf("Expected Minor number 0 in item 1, found %d", v.Minor)
	}
	if v.Revision != 1 {
		t.Errorf("Expected Revision number 1 in item 0, found %d", v.Major)
	}
	if v.UpdateId == "" {
		t.Errorf("No update id read in item 1")
	}
}

// Tests unmarshalling DbUpdte from JSON
func TestUpgradeStatementsJSON(t *testing.T) {
	blob :=  `
		{
			"id": "1.0.0",
			"statements": [
				{
					"id": "db_version-create",
					"statement": "CREATE TABLE ams_db_version (\nmajor INTEGER NOT NULL,\nminor INTEGER NOT NULL,\nrevision INTEGER NOT NULL,\nupdated TIMESTAMP NOT NULL,\nCONSTRAINT app_version PRIMARY KEY (\nmajor, minor, revision\n)\n);"
				},
				{
					"id": "translations-create",
					"statement": "CREATE TABLE ams_translations (\nid CHAR(36) PRIMARY KEY,\norg_id VARCHAR(64) NOT NULL,\nlanguage CHAR(2) NOT NULL,\ncountry CHAR(2),\ncontent JSON NOT NULL,\nCONSTRAINT idx_translations_01 UNIQUE (\norg_id, language, country\n)\n);"
				},
				{
					"id": "validations-create",
					"statement": "CREATE TABLE ams_validations (\nid CHAR(36) PRIMARY KEY,\norg_id VARCHAR(64) NOT NULL,\ncontent JSON NOT NULL,\nCONSTRAINT idx_validations_01 UNIQUE (\norg_id\n)\n);"
				}
			]
		}`

	upgrade := new(DbUpdate)
	err := json.Unmarshal([]byte(blob), upgrade)
	validateUpgradeStatements(t, upgrade, err)
}

// Tests unmarshalling DbUpdte from XML
func TestUpgradeStatementsXML(t *testing.T) {
	blob :=  `
		<dbupdate id="1.0.0">
			<!-- Version -->
			<statement id="db_version-create">
				CREATE TABLE ams_db_version (
					major INTEGER NOT NULL,
					minor INTEGER NOT NULL,
					revision INTEGER NOT NULL,
					updated TIMESTAMP NOT NULL,
					CONSTRAINT app_version PRIMARY KEY (
						major, minor, revision
					)
				);
			</statement>
		
			<!-- Translations -->
			<statement id="translations-create">
				CREATE TABLE ams_translations (
					id CHAR(36) PRIMARY KEY,
					org_id VARCHAR(64) NOT NULL,
					language CHAR(2) NOT NULL,
					country CHAR(2),
					content JSON NOT NULL,
		
					CONSTRAINT idx_translations_01 UNIQUE (
						org_id, language, country
					)
				);
			</statement>
		
			<!-- Validations -->
			<statement id="validations-create">
				CREATE TABLE ams_validations (
					id CHAR(36) PRIMARY KEY,
					org_id VARCHAR(64) NOT NULL,
					content JSON NOT NULL,
		
					CONSTRAINT idx_validations_01 UNIQUE (
						org_id
					)
				);
			</statement>
		</dbupdate>`

	upgrade := new(DbUpdate)
	err := xml.Unmarshal([]byte(blob), upgrade)
	validateUpgradeStatements(t, upgrade, err)
}

// Tests unmarshalling DbUpdte from YAML
func TestUpgradeStatementsYAML(t *testing.T) {
	blob :=  `
id: 1.0.0
statements:
  # Version
  - id: db_version-create
    statement: |
      CREATE TABLE ams_db_version (
        major INTEGER NOT NULL,
        minor INTEGER NOT NULL,
        revision INTEGER NOT NULL,
        updated TIMESTAMP NOT NULL,
        CONSTRAINT app_version PRIMARY KEY (
          major, minor, revision
        )
      );
  # Translations
  - id: translations-create
    statement: |
      CREATE TABLE ams_translations (
        id CHAR(36) PRIMARY KEY,
        org_id VARCHAR(64) NOT NULL,
        language CHAR(2) NOT NULL,
        country CHAR(2),
        content JSON NOT NULL,

        CONSTRAINT idx_translations_01 UNIQUE (
          org_id, language, country
        )
      );
  # Validations
  - id: validations-create
    statement: |
      CREATE TABLE ams_validations (
        id CHAR(36) PRIMARY KEY,
        org_id VARCHAR(64) NOT NULL,
        content JSON NOT NULL,

        CONSTRAINT idx_validations_01 UNIQUE (
          org_id
        )
      );`

	upgrade := new(DbUpdate)
	err := yaml.Unmarshal([]byte(blob), upgrade)
	validateUpgradeStatements(t, upgrade, err)
}

// Validates DbUdpdate data un-marshaled from the various formats.
func validateUpgradeStatements(t *testing.T, upgrade *DbUpdate, err error) {
	expectedIds := []string{
		"db_version-create",
		"translations-create",
		"validations-create",
	}

	if err != nil {
		t.Errorf("Error unmarshalling upgrade Statements: %s", err)
	}

	if upgrade.Id != "1.0.0" {
		t.Errorf("Expected upgrade versions ID 1.0.0, got \"%s\"", upgrade.Id)
	}

	statements := upgrade.Statements
	i := len(statements)
	if i != 3 {
		t.Errorf("Expected 3 upgrade Statements, but found %d", i)
	}

	for j, stmt := range statements {
		if stmt.Id != expectedIds[j] {
			t.Errorf("Expected ID \"%s\" for statement %d but found \"%s\"", expectedIds[j], j, stmt.Id)
		}
		if stmt.Statement == "" {
			t.Errorf("Invalid SQL statement for statement %d", j)
		}
	}
}
