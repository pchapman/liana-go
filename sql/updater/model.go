package updater

// A schema version which includes the unique ID of the update statements package which must be run in order to upgrade
// a DB to the version.
type DbVersion struct {
	Major int8 `json:"major" xml:"major,attr" yaml:"major"`
	Minor int8 `json:"minor" xml:"minor,attr" yaml:"minor"`
	Revision int8 `json:"revision" xml:"revision,attr" yaml:"revision"`
	UpdateId string `json:"updateid" xml:"updateid,attr" yaml:"updateid"`
}

// Prepared statements which may be used to insert a version into the versions table once the proper upgrades have been
// run as well as a prepared statement used to query for the latest schema version.  The insert statement is expected
// to accept 3 arguments: the major, minor and revision numbers of the newly upgraded version, in that order.  The
// select statement is expected to take no arguments and return the latest major, minor and revision, in that order.
type DbVersionTable struct {
	Insert string `json:"insert" xml:"insert" yaml:"insert"`
	Select string `json:"select" xml:"select" yaml:"select"`
}

// An object that lists SQL statements for querying for current schema version and inserting the latest schema version
// after update.  It also contains an ordered list of versions from oldest to newest such that a database of any version
// can be brought up to the latest by applying the proper upgrades in order.
type DbVersions struct {
	VersionTable DbVersionTable `json:"versionTable" xml:"versionTable" yaml:"versionTable"`
	Versions []DbVersion `json:"versions" xml:"version" yaml:"versions"`
}

// A single statement which must be run in the process of updating a database.  It may be schema modification statements
// such as table creates or it may be data change statement, such as inserts, updates or deletes.  The ID is provided
// so that if an error occurs during update, the exact statement that resulted in an error can be logged for
// troubleshooting.
type DbChangeStatement struct {
	Id string `json:"id" xml:"id,attr" yaml:"id"`
	Statement string `json:"statement" xml:",chardata" yaml:"statement"`
}

// Contains a sequential list of statements to be executed against the database in order to upgrade the schema.
type DbUpdate struct {
	Id string `json:"id" xml:"id,attr" yaml:"id"`
	Statements []DbChangeStatement `json:"statements" xml:"statement" yaml:"statements"`
}
