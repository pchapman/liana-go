package updater

import (
    "database/sql"
    "fmt"
    "log"
)

// A function which will return a DbVersions object that is used to determine what the current version of the DB is and
// what upgrades must be run to get it to the proper version, if it is not up to date.
type DbVersionsProvider func() DbVersions

// A function which will return the DBUpdate object for the given unique ID found in the DBVersions provided by
// DBVersionsProvider.  This DBUpdate object will be used to supply a sequential list of SQL statements which must
// be run to upgrade the DB to a certain version.
type DbUpdatesProvider func(updateId string) DbUpdate

// Checks a database' version according to information provided by the versionsProvider.  If it is found that the DB
// needs to be upgraded, 1 or more calls will be made to the pdatesPRovider to get the necessary update statements for
// updating the database.  If and error occurs while attempting to upgrade the DB, all the upgrade statements are rolled
// back and the version and statement ID is logged along with the error from the database system.  If this happens and
// panicOnError is true, the application will panic.  Else, the error is returned.  A null return value indicates success.
func UpdateDb(db *sql.DB, versionsProvider DbVersionsProvider, updatesProvider DbUpdatesProvider, panicOnError bool) error {
    var (
        major int8
        minor int8
        revision int8
        doUpgrade bool
    )

    versions := versionsProvider()
    err := db.QueryRow(versions.VersionTable.Select).Scan(&major, &minor, &revision)
    if err != nil {
        log.Printf("An error was returned attempting to determin the current version of the database.  It is assumed the table does not exist and the database needs to be initialized from the oldest version forward. %s", err)
        major = -128
        minor = -128
        revision = -128
    }

    for _, version := range versions.Versions {
        doUpgrade = false
        if major < version.Major {
            doUpgrade = true
        } else if major == version.Major {
            if minor < version.Minor {
                doUpgrade = true
            } else if minor == version.Minor {
                doUpgrade = revision < version.Revision
            }
        }
        if doUpgrade {
            if !executeUpdate(db, versions, version, updatesProvider, panicOnError) {
                return fmt.Errorf("upgrades aborted due to errors during upgrade to version %d.%d.%d", version.Major, version.Minor, version.Revision)
            }
        }
    }

    return nil
}

// Applies the update to the database by executing its statements in order.  Should an error be thrown in the execution
// of one of the statements, it is handled according to panicOnError and the methiod returns false.
func executeUpdate(db *sql.DB, versions DbVersions, version DbVersion, provider DbUpdatesProvider, panicOnError bool) bool {
    updateId := version.UpdateId
    log.Printf("Attempting upgrade %s", updateId)

    tx, err := db.Begin()
    if err != nil {
        if panicOnError {
            panic(fmt.Sprintf("error starting transation"))
        } else {
            return false
        }
    }

    update := provider(updateId)
    for _, stmt := range update.Statements {
        log.Printf("Attempting statement %s of update %s", stmt.Id, updateId)
        _, err = tx.Exec(stmt.Statement)
        if err != nil {
            err = tx.Rollback()
            if err != nil {
                log.Printf("Error rolling back update %s: %s", updateId, err)
            }
            msg := fmt.Sprintf("Error executing statement %s of update %s: %s", stmt.Id, updateId, err)
            if panicOnError {
                panic(msg)
            } else {
                log.Print(msg)
                return false
            }
        }
        log.Printf("Statement %s of update %s succeeded", stmt.Id, updateId)
    }

    ps, err := tx.Prepare(versions.VersionTable.Insert)
    if err == nil {
        _, err = ps.Exec(&version.Major, &version.Minor, &version.Revision)
    }
    if err != nil {
        err = tx.Rollback()
        if err != nil {
            log.Printf("Error rolling back update %s: %s", updateId, err)
        }
        msg := fmt.Sprintf("Error inserting version for update %s: %s", updateId, err)
        if panicOnError {
            panic(msg)
        } else {
            log.Print(msg)
            return false
        }
    }

    err = tx.Commit()
    if err != nil {
        msg := fmt.Sprintf("Error committing statements of update %s: %s", updateId, err)
        if panicOnError {
            panic(msg)
        } else {
            log.Print(msg)
            return false
        }
    }

    log.Printf("Update %s completed successfully", updateId)
    return true
}
